# Nautilus Kill Zone Identifier
Nautilus Kill Zone Identifier is an extension for the file manager nautilus (aka File) that remove automaticaly
Microsoft Windows "filename:Zone.Identifier" files.
Zone.Identifier are ADSs (alternate data stream) created by Internet Explorer and other browsers to mark files downloaded from external sites as possibly unsafe.
For more information read this [Wikipedia page](https://en.wikipedia.org/wiki/NTFS#Alternate_data_stream_(ADS)).

## Dependencies
- `glib-2.0 >= 2.44`
- `libnautilus-extension ≥ 2.12.0`  or `libnautilus-extension-4 ≥ 43`

## Building and Installing
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

In order to build you will need to have istalled gcc, git, meson and ninja.

For a regular use these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/nautilus-kill-zone-identifier.git
cd nautilus-kill-zone-identifier
meson compile -C _build
meson install -C _build
```

## License
Nautilus Kill Zone Identifier - Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>

Nautilus Kill Zone Identifier is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Nautilus Kill Zone Identifier is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Nautilus Kill Zone Identifier. If not, see <https://www.gnu.org/licenses/>.

