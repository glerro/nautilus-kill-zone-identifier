/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Nautilus Kill Zone Identifier.
 * https://gitlab.gnome.org/glerro/nautilus-kill-zone-identifier
 *
 * nautilus-kill-zone-identifier.h
 *
 * Copyright (c) 2022-2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Nautilus Kill Zone Identifier is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nautilus Kill Zone Identifier is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Nautilus Kill Zone Identifier. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022-2023 Gianni Lerro <glerro@pm.me>
 */

#pragma once

#ifndef NAUTILUS_KILL_ZONE_IDENTIFIER_H
#define NAUTILUS_KILL_ZONE_IDENTIFIER_H

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_KILL_ZONE_IDENTIFIER_TYPE  (nautilus_kill_zone_identifier_get_type ())
#define NAUTILUS_KILL_ZONE_IDENTIFIER(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_KILL_ZONE_IDENTIFIER_TYPE, NautilusKillZoneIdentifier))
#define NAUTILUS_KILL_ZONE_IDENTIFIER_IS_NAUTILUS(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_KILL_ZONE_IDENTIFIER_TYPE))

typedef struct _NautilusKillZoneIdentifier NautilusKillZoneIdentifier;
typedef struct _NautilusKillZoneIdentifierClass NautilusKillZoneIdentifierClass;

struct _NautilusKillZoneIdentifier
{
    GObject __parent;
};

struct _NautilusKillZoneIdentifierClass
{
    GObjectClass __parent;
};

void  nautilus_kill_zone_identifier_register_type (GTypeModule *module);
GType nautilus_kill_zone_identifier_get_type (void);

G_END_DECLS

#endif

